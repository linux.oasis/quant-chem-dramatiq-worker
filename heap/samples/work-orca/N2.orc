! B3LYP/G 6-31G(d,p) Opt NumFreq MiniPrint
%geom MaxIter 500 end
%scf MaxIter 500 end
%freq Temp 298.15, 323.15, 348.15, 373.15, 398.15 end
* xyz 0 1
N          -1.21660         0.13350         0.00000
N          -2.30860         0.13350         0.00000

*
%pal nprocs 2 end
