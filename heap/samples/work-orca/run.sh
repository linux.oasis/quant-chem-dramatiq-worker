#!/bin/bash

ORCA="/app/work/orca-5.0.4"
export ORCA
export PATH=:$ORCA:$PATH
export LD_LIBRARY_PATH=$ORCA:$LD_LIBRARY_PATH
export OMPI_ALLOW_RUN_AS_ROOT=1
export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1

$ORCA/orca N2.orc > N2.out
