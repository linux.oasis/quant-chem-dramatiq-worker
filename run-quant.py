#!/usr/bin/python3
# -*- coding: utf-8 -*-

import time
import dramatiq
import tasks
from dramatiq.brokers.redis import RedisBroker
from dramatiq.results import Results
from dramatiq.results.backends import RedisBackend
import requests
import sys

def upload_file_to_ipfs(file_path):
    url = 'http://localhost:15001/api/v0/add'
    files = {'file': open(file_path, 'rb')}
    response = requests.post(url, files=files)
    json_response = response.json()
    cid = json_response['Hash']
    print(f'File {file_path} send to IPFS:{cid}')
    return cid

def download_file_from_ipfs(cid, save_path):
    url = f"http://localhost:18080/ipfs/{cid}"
    response = requests.get(url, stream=True)
    with open(save_path, "wb") as file :
        for chunk in response.iter_content(chunk_size=1024) :
            file.write(chunk)
#    response = requests.get(url)
#    with open(save_path, 'wb') as f:
#        f.write(response.content)
    print(f'Data from IPFS:{cid} saved to {save_path}')

backend = RedisBackend()
broker = RedisBroker(host='127.0.0.1', port=6379)
broker.declare_queue("default")
broker.add_middleware(Results(backend=backend))
dramatiq.set_broker(broker)

def get_result(r) :
    while True:
        try:
            res = r.get_result(backend=backend)
            break
        except dramatiq.results.errors.ResultMissing:
            time.sleep(1)
    return res

inp_cid = upload_file_to_ipfs(sys.argv[1])
r = tasks.work.send(inp_cid)
time.sleep(3)
out_cid = get_result(r)
download_file_from_ipfs(out_cid, sys.argv[2])

#download_file_from_ipfs('QmYCLYwYtZ18kTeH5CpvpLWcTzzBsj1ZQXCAFx1hrdkgBF', 'r.tar.gz')
