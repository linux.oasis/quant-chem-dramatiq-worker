#!/usr/bin/python3
# -*- coding: utf-8 -*-

import dramatiq
from dramatiq.brokers.redis import RedisBroker
from dramatiq.results import Results
from dramatiq.results.backends import RedisBackend

broker = RedisBroker(url="redis://localhost:6379/0")

backend = RedisBackend()
broker.declare_queue("default")
broker.add_middleware(Results(backend=backend))

dramatiq.set_broker(broker)

#@dramatiq.on_message()
#def handle_result(message):
#    if "result" in message:
#        print("Результат выполнения задачи:", message["result"])

@dramatiq.actor(store_results=True)
def add(x, y):
    print(f"Adding {x} + {y}")
    return x + y

@dramatiq.actor(store_results=True)
def multiply(x, y):
    print(f"Multiplying {x} * {y}")
    return x * y
