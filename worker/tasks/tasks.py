#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import dramatiq
from dramatiq.brokers.redis import RedisBroker
from dramatiq.results import Results
from dramatiq.results.backends import RedisBackend
import requests
import subprocess

#with open('/app/work/os.environ', 'w') as f: print(os.environ, file=f)

broker_url = os.environ.get('BROKER', 'redis://localhost:6379/0')
broker = RedisBroker(url=broker_url)

backend = RedisBackend()
broker.declare_queue("default")
broker.add_middleware(Results(backend=backend))

dramatiq.set_broker(broker)

@dramatiq.actor(store_results=True)
def add(x, y):
    print(f"Adding {x} + {y}")
    return x + y

@dramatiq.actor(store_results=True)
def multiply(x, y):
    print(f"Multiplying {x} * {y}")
    return x * y

ipfs = 'localhost:5001'

def download_from_ipfs(ipfs_hash, fname):
    data = requests.post(f'http://{ipfs}/api/v0/cat', params={'arg' : ipfs_hash, 'encoding':'json'})
    with open(fname, "wb") as f: f.write(data.content)
    return len(data.content)

def upload_to_ipfs(fname):
    r = requests.post(f'http://{ipfs}/api/v0/add', params={'metadata': 'false'}, files={'file': open(fname, 'rb')})
    return r.json()['Hash']

@dramatiq.actor(store_results=True)
def work(ipfs_hash):
    n = download_from_ipfs(ipfs_hash, '/app/work/works.tar.gz')
    print(f'Download {n} bytes.')
    r = subprocess.run(['bash', '/app/work/run-work.sh', '/app/work/works.tar.gz'])
    print('Код возврата:', r.returncode)
    print('Стандартный вывод:', r.stdout)
    print('Стандартный вывод ошибок:', r.stderr)
    r_hash = upload_to_ipfs('/app/work/r.tar.gz')
    print('ipfs_hash: %s.' % r_hash)
    r = subprocess.run(['/usr/bin/rm', '/app/work/r.tar.gz'])
    return r_hash
