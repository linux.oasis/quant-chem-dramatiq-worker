#!/usr/bin/python3
# -*- coding: utf-8 -*-

import time
import dramatiq
import tasks
from dramatiq.brokers.redis import RedisBroker
from dramatiq.results import Results
from dramatiq.results.backends import RedisBackend

backend = RedisBackend()
broker = RedisBroker(host='127.0.0.1', port=6379)
broker.declare_queue("default")
broker.add_middleware(Results(backend=backend))
dramatiq.set_broker(broker)

def get_result(r) :
    while True:
        try:
            res = r.get_result(backend=backend)
            break
        except dramatiq.results.errors.ResultMissing:
            time.sleep(1)
    return res

#for i in range(1,5) :
#    r = tasks.add.send(i,i)
#    print(get_result(r))

#rl = []
#for i in range(1,11) :
#    rl.append(tasks.add.send(i,i))

#time.sleep(3)
#for r in rl : 
#    print(get_result(r))

r = tasks.work.send('QmPheKg9WpWhQkrW3pSQ2sdrmjhEAwDEPU8eAYeE6iA4Tx')
time.sleep(3)
print(get_result(r))

r = tasks.work.send('QmasoDnhPksu8Aew15m295VJJXQfDZVxRGcBByqHwPvhYs')
time.sleep(3)
print(get_result(r))
