#!/bin/bash

ls /app/work/g16-c01
tar -xzvf $1 -C /app/work/run
rm $1
cd /app/work/run
./run.sh
cd ..
tar -czvf /app/work/r.tar.gz -C /app/work run
rm -rf /app/work/run/*
